#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <cmath>
// #include <ctype.h>
// #include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <sstream>
// #include <algorithm>
// #include <regex>
using namespace std;
typedef long long ll;
typedef unsigned int uint;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int _ = 0; _ < n; _++)
#define loopi(n) for (int i = 0; i < n; i++)
#define loopi1(n) for (int i = 1; i <= n; i++)
#define loopj(n) for (int j = 0; j < n; j++)
#define loopj1(n) for (int j = 1; j <= n; j++)
#define mbr(n,m) get<m>(n)
#define DEBUG 0
#define CASES 0
#define endl "\n"
#define all(n) n.begin(), n.end()

const int maxSize = 2010, direction = 8;
bool location[maxSize][maxSize], given[maxSize][maxSize];
uint people = 0, row, col;
const int dirX[] = {-1, -1, 0, 1, 1, 1, 0, -1},
          dirY[] = {0, -1, -1, -1, 0, 1, 1, 1};
uint leastCandle() {
    uint candle = 0, visited = 0; stack<pi> lookup; pi pos; bool lighted = false;
    for (uint posY = 0; posY < row; posY++) for (uint posX = 0; posX < col; posX++) {
        if (!given[pos.nd][pos.st]) lookup.push({posX, posY}); lighted = false;
        while (!lookup.empty()) {
            pos = lookup.top(); lookup.pop();
            if (given[pos.nd][pos.st]) continue;
            if (location[pos.nd][pos.st]) {
                if (!lighted) { candle += 1; lighted = true; }
                people -= 1; if (people == 0) return candle;
                loopi(direction) {
                    if (pos.st + dirX[i] < 0 || pos.st + dirX[i] >= col) continue;
                    if (pos.nd + dirY[i] < 0 || pos.nd + dirY[i] >= row) continue;
                    if (!given[pos.nd][pos.st]) lookup.push({pos.st + dirX[i], pos.nd + dirY[i]});
                }
            } given[pos.nd][pos.st] = true; visited += 1;
            if (visited == row * col) return candle;
        }
    } return candle;
}
void real_process() {
    cin >> row >> col; string line;
    loopi(row) {
        cin >> line;
        loopj(line.length()) {
            location[i][j] = line[j] == '1';
            if (location[i][j]) people += 1;
        }
    } cout << leastCandle() << endl;
    // 19 / 20 - Time limit exceed 1 case | Memory exceed 1 MB 1 case
    return;
}

/* const int maxSize = 2010, bound = 3;
bool location[bound][maxSize];
void real_process() {
    uint row, col, candle = 0; string line; bool lighted, transfered;
    cin >> row >> col;
    loopi(row) {
        cin >> line; loopj(line.length()) location[i % bound][j] = line[j] == '1';
        lighted = false, transfered = false;
        loopj(col) {
            if (!location[i % bound][j]) {
                lighted = transfered = false;
                continue;
            } else if (!lighted) {
                lighted = true;
                candle += 1;
                if (!transfered) for (int k = -1; k <= 1; k++) {
                    if (j + k < 0 || j + k >= col) continue;
                    if (location[(i - 1) % bound][j + k] || location[(i - 2) % bound][j + k]) {
                        candle -= 1;
                        transfered = true;
                        break;
                    }
                }
            }
        }
    } cout << candle << endl;
    return;
} */

int main() {
    #ifndef DEBUG
    freopen("test_case.in", "r", stdin);
    freopen("test_case.out", "w", stdout);
    #endif
    cin.tie(nullptr) -> ios::sync_with_stdio(false);
    int t(1);
    if (CASES) cin >> t;
    while(t--) real_process();
    return 0;
}