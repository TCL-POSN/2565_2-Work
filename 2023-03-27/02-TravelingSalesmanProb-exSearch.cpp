#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <cmath>
// #include <ctype.h>
// #include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <sstream>
// #include <algorithm>
// #include <regex>
using namespace std;
typedef long long ll;
typedef unsigned int uint;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int _ = 0; _ < n; _++)
#define loopi(n) for (int i = 0; i < n; i++)
#define loopi1(n) for (int i = 1; i <= n; i++)
#define loopj(n) for (int j = 0; j < n; j++)
#define loopj1(n) for (int j = 1; j <= n; j++)
#define mbr(n,m) get<m>(n)
#define DEBUG 0
#define CASES 0
#define endl "\n"
#define all(n) n.begin(), n.end()

uint places, paths;
int pathway(vector<vector<int>> &path) {
    uint shortest = LM, stopping = 0, dist, pos;
    vector<int> visiting;
    loopi(places) if (i != stopping)
        visiting.push_back(i);
    do {
        dist = 0; pos = stopping;
        loopi(visiting.size()) {
            if (!path[pos][visiting[i]]) {
                dist = LM;
                break;
            } dist += path[pos][visiting[i]];
            pos = visiting[i];
        } if (dist != LM && path[pos][stopping]) {
            dist += path[pos][stopping];
            shortest = min(shortest, dist);
        }
    } while (next_permutation(all(visiting)));
    return shortest;
}
void real_process() {
    cin >> places >> paths;
    vector<vector<int>> path(places, vector<int>(places, 0));
    loopi(paths) {
        int dept, tmnl, dist;
        cin >> dept >> tmnl >> dist;
        path[dept][tmnl] = dist;
        path[tmnl][dept] = dist;
    } cout << pathway(path) << endl;
    return;
}

int main() {
    #ifndef DEBUG
    freopen("test_case.in", "r", stdin);
    freopen("test_case.out", "w", stdout);
    #endif
    cin.tie(nullptr) -> ios::sync_with_stdio(false);
    int t(1);
    if (CASES) cin >> t;
    while(t--) real_process();
    return 0;
}