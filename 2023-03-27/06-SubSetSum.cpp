#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <cmath>
// #include <ctype.h>
// #include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <sstream>
// #include <algorithm>
// #include <regex>
using namespace std;
typedef long long ll;
typedef unsigned int uint;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int _ = 0; _ < n; _++)
#define loopi(n) for (int i = 0; i < n; i++)
#define loopi1(n) for (int i = 1; i <= n; i++)
#define loopj(n) for (int j = 0; j < n; j++)
#define loopj1(n) for (int j = 1; j <= n; j++)
#define mbr(n,m) get<m>(n)
#define DEBUG 0
#define CASES 0
#define endl "\n"
#define all(n) n.begin(), n.end()

vector<int> numArr; int amount, wanted, tmp_in;
int subsetSum(int search, int sum) {
    // Recheck
    pii parent;
    stack<pii> lookup; lookup.push({search, {sum, 0}});
    deque<int> target; int removal = 0;
    while (true) {
        parent = lookup.top();
        search = parent.st; sum = parent.nd.st;
        if (sum == 0) break;
        if (removal > parent.nd.nd) loop (removal - parent.nd.nd) target.pop_back();
        removal = parent.nd.nd;
        // printf("[Debugger]~$ Left: %d\tSeek: %d (%d)\thisory: %d\tstack: %d\n", sum, amount - search, numArr[amount - search], removal, lookup.size());
        if (!lookup.empty()) lookup.pop();
        if (search == 0 || sum < 0) continue;
        if (numArr[amount - search] > sum || search + 1 == amount) lookup.push({search - 1, {sum, removal}});
        target.push_back(numArr[amount - search]);
        lookup.push({search - 1, {sum - numArr[amount - search], removal + 1}});
    } if (sum != 0) {
        if (search != amount) cout << "No subset" << endl;
        return -1;
    } int first = target.front();
    while (!target.empty()) {
        cout << target.front() << " ";
        target.pop_front();
    } cout << endl;
    auto index = find(numArr.begin(), numArr.end(), first);
    return index - numArr.begin();
}
void real_process() {
    cin >> amount >> wanted;
    loop(amount) { cin >> tmp_in; numArr.push_back(tmp_in); }
    int previous = 0, tmp_val = 0;
    loopi(amount) {
        tmp_val = subsetSum(amount - i, wanted);
        if (tmp_val < 0) break;
        else if (tmp_val > 0) i += tmp_val - previous - 1;
        previous = tmp_val;
    } return;

    /***
     * Case 6 9 | 3 34 4 12 5 2 -> 3 4 2, 4 5 : Fine
     * Case 5 5 | 1 2 3 4 6 -> 1 5, 2 3 : Fail
     * [Debugger]~$ Left: 1    Seek: 3 (4)     hisory: 2       stack: 1
     ***/
}

int main() {
    #ifndef DEBUG
    freopen("test_case.in", "r", stdin);
    freopen("test_case.out", "w", stdout);
    #endif
    cin.tie(nullptr) -> ios::sync_with_stdio(false);
    int t(1);
    if (CASES) cin >> t;
    while(t--) real_process();
    return 0;
}