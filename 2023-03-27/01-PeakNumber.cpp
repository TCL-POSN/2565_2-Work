#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <cmath>
// #include <ctype.h>
// #include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <sstream>
// #include <algorithm>
// #include <regex>
using namespace std;
typedef long long ll;
typedef unsigned int uint;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int _ = 0; _ < n; _++)
#define loopi(n) for (int i = 0; i < n; i++)
#define loopi1(n) for (int i = 1; i <= n; i++)
#define loopj(n) for (int j = 0; j < n; j++)
#define loopj1(n) for (int j = 1; j <= n; j++)
#define mbr(n,m) get<m>(n)
#define DEBUG 0
#define CASES 0
#define endl "\n"
#define all(n) n.begin(), n.end()

void real_process() {
    int amount, store[] = {0, 0, 0}; cin >> amount;
    loopi(amount) {
        // Method 1
        if (i >= 2 && i < amount - 1) {
            if (store[0] < store[1] && store[1] > store[2]) cout << i - 2 << " ";
        } else if (i == 1 && store[0] > store[1]) cout << 0 << " ";
        else if (i == amount - 1 && store[1] < store[2]) cout << i - 1 << " ";
        store[0] = store[1];
        store[1] = store[2];
        cin >> store[2];
        // Method 2: Missing last answer
        /* cin >> store[i % 3];
        if (i >= 2 && i < amount -1) {
            if (store[(i - 2) % 3] < store[(i - 1) % 3] && store[(i - 1) % 3] > store[i % 3]) cout << i - 2 << " ";
        } else if (i == 1 && store[0] > store[1]) cout << 0 << " ";
        else if (i == amount - 1 && store[(i - 1) % 3] < store[i % 3]) cout << i-1 << " "; */
    } cout << endl;
    return;
}

int main() {
    #ifndef DEBUG
    freopen("test_case.in", "r", stdin);
    freopen("test_case.out", "w", stdout);
    #endif
    cin.tie(nullptr) -> ios::sync_with_stdio(false);
    int t(1);
    if (CASES) cin >> t;
    while(t--) real_process();
    return 0;
}