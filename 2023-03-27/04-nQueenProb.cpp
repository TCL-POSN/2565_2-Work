#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <cmath>
// #include <ctype.h>
// #include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <sstream>
// #include <algorithm>
// #include <regex>
using namespace std;
typedef long long ll;
typedef unsigned int uint;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int _ = 0; _ < n; _++)
#define loopi(n) for (int i = 0; i < n; i++)
#define loopi1(n) for (int i = 1; i <= n; i++)
#define loopj(n) for (int j = 0; j < n; j++)
#define loopj1(n) for (int j = 1; j <= n; j++)
#define mbr(n,m) get<m>(n)
#define DEBUG 0
#define CASES 0
#define endl "\n"
#define all(n) n.begin(), n.end()

vector<bool> boardCol, boardDiag1, boardDiag2; uint maxQueen;
void calcQueen(uint *boardSize, uint row = 0) {
    // Fix
    cout << "[Debugger]~$ Row = " << row << endl;
    /* if (*boardSize < 4) {
        switch (*boardSize) {
            case 0: maxQueen = 0; break;
            case 1: case 2: maxQueen = 1; break;
            case 3: maxQueen = 2; break;
        } return;
    } else */ if (row == *boardSize) { maxQueen += 1; return; }
    else if (row == 0) {
        maxQueen = 0;
        boardCol.assign(*boardSize, false);
        boardDiag1.assign(*boardSize, false);
        boardDiag2.assign(*boardSize, false);
    } loopi(*boardSize) {
        cout << "[Debugger]~$ row = " << row << "\ti = " << i << "\tbSize = " << *boardSize << endl;
        cout << "[Debugger]~$ boardCol[" << i << "]: " << boardCol[i] << "\t";
        cout << "boardDiag1[" << i + row << "]: " << boardDiag1[i + row] << "\t";
        cout << "boardDiag2[" << i - row + *boardSize - 1 << "]: " << boardDiag2[i - row + *boardSize - 1] << endl;
        if (boardCol[i] || boardDiag1[i + row] || boardDiag2[i - row + *boardSize - 1]) continue;
        boardCol[i] = boardDiag1[i + row] = boardDiag2[i - row + *boardSize - 1] = true;
        calcQueen(boardSize, row + 1);
        boardCol[i] = boardDiag1[i + row] = boardDiag2[i - row + *boardSize - 1] = false;
    }
}
void real_process() {
    uint boardSize; cin >> boardSize;
    calcQueen(&boardSize);
    cout << maxQueen << endl;
    return;
}

int main() {
    #ifndef DEBUG
    freopen("test_case.in", "r", stdin);
    freopen("test_case.out", "w", stdout);
    #endif
    cin.tie(nullptr) -> ios::sync_with_stdio(false);
    int t(1);
    if (CASES) cin >> t;
    while(t--) real_process();
    return 0;
}