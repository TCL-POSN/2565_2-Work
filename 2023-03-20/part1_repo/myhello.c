#include <stdio.h>
#include <stdlib.h>

int main() {
    char c, name[100];
    int i = 0;
    c = (char) getc(stdin);
    while ((c != EOF) && (c != '\n') && (i<100)) {
       name[i++] = c;
       c = (char) getc(stdin);
    } // end while
    name[i] = '\0';
    printf("Hello C : %s\n", name);
    return 0;
} // end main()