#include<iostream>
#include<string>
#include<vector>
using namespace std;

int main() {
    std::string              line;
    std::vector<std::string> lines;

    cout << "Enter Your Text here (terminated with twice <enter>s):\n";
    while (getline(cin, line) && !line.empty()) {
        lines.push_back(line);
    }

    cout << "\n\nWe have received the following text ("
         << lines.size()
         << " lines) from you: \n";
    for (auto const& l : lines) {
        cout << l << endl;
    }

    return 0;
} // end main()
