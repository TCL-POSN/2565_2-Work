#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <cmath>
// #include <ctype.h>
// #include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <sstream>
// #include <algorithm>
// #include <regex>
using namespace std;
typedef long long ll;
typedef unsigned int uint;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int _ = 0; _ < n; _++)
#define loopi(n) for (int i = 0; i < n; i++)
#define loopi1(n) for (int i = 1; i <= n; i++)
#define loopj(n) for (int j = 0; j < n; j++)
#define loopj1(n) for (int j = 1; j <= n; j++)
#define mbr(n,m) get<m>(n)
#define DEBUG 0
#define CASES 0
#define endl "\n"
#define all(n) n.begin(), n.end()

vector<int> numArr; uint amount;
uint brute_force() {
    if (amount == 0) return 0;
    uint longest = 0, length = 0; int lowest;
    set<int> untrue;
    loopi(amount) {
        if (untrue.find(numArr[i]) != untrue.end()) continue;
        lowest = numArr[i]; untrue.insert(numArr[i]);
        length = 1;
        loopj1(amount - i - 1) {
            if (numArr[i + j] <= lowest) continue;
            lowest = numArr[i + j]; untrue.insert(numArr[i + j]);
            length += 1;
        } longest = max(longest, length);
    } return longest;
    // Case skip one for glorious purpose failed
}
int maxIdx(vector<int> &seq, int right, int *value) {
    int middle, left = -1;
    while (right - 1 > left) {
        middle = left + (right - left) / 2;
        if (seq[middle] >= *value) right = middle;
        else left = middle;
    } return right;
}
uint DynamicProgramming() {
    if (amount == 0) return 0;
    vector<int> seq(amount, 0); seq[0] = numArr[0];
    uint length = 1;
    loopi1(amount - 1) {
        if (numArr[i] < seq[0]) seq[0] = numArr[i];
        else if (numArr[i] > seq[length - 1]) seq[length++] = numArr[i];
        else seq[maxIdx(seq, length - 1, &numArr[i])] = numArr[i];
    } return length;
}
void real_process() {
    int tmp_in;
    cin >> amount;
    loop(amount) { cin >> tmp_in; numArr.push_back(tmp_in); }
    // cout << brute_force() << endl;
    cout << DynamicProgramming() << endl;
    return;
}

int main() {
    #ifndef DEBUG
    freopen("test_case.in", "r", stdin);
    freopen("test_case.out", "w", stdout);
    #endif
    cin.tie(nullptr) -> ios::sync_with_stdio(false);
    int t(1);
    if (CASES) cin >> t;
    while(t--) real_process();
    return 0;
}