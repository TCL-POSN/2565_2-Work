#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <cmath>
// #include <ctype.h>
// #include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <sstream>
// #include <algorithm>
// #include <regex>
using namespace std;
typedef long long ll;
typedef unsigned int uint;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int _ = 0; _ < n; _++)
#define loopi(n) for (int i = 0; i < n; i++)
#define loopi1(n) for (int i = 1; i <= n; i++)
#define loopj(n) for (int j = 0; j < n; j++)
#define loopj1(n) for (int j = 1; j <= n; j++)
#define mbr(n,m) get<m>(n)
#define DEBUG 0
#define CASES 0
#define endl "\n"
#define all(n) n.begin(), n.end()

const int maxSize = 1000;
uint dp[maxSize][maxSize];
uint Cnr(uint n, uint r) {
    if (r == 1) return n;
    if (r == 0 || r == n) return 1;
    uint answer = n; r = min(r, n - r);
    loopi1(r - 1) answer *= (n - i) / (r - i + 1);
    return answer;
}
uint binomial(uint n, uint k) {
    if (dp[n][k] == 0) {
        if (k == 0 || n == k) dp[n][k] = 1;
        else dp[n][k] = binomial(n - 1, k - 1) + binomial(n - 1, k);
    } return dp[n][k];
}
void real_process() {
    uint n, k; cin >> n >> k;
    dp[1][1] = Cnr(1, 1);
    cout << binomial(n, k) << endl;
    return;
}

int main() {
    #ifndef DEBUG
    freopen("test_case.in", "r", stdin);
    freopen("test_case.out", "w", stdout);
    #endif
    cin.tie(nullptr) -> ios::sync_with_stdio(false);
    int t(1);
    if (CASES) cin >> t;
    while(t--) real_process();
    return 0;
}