#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <cmath>
// #include <ctype.h>
// #include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <sstream>
// #include <algorithm>
// #include <regex>
using namespace std;
typedef long long ll;
typedef unsigned int uint;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int _ = 0; _ < n; _++)
#define loopi(n) for (int i = 0; i < n; i++)
#define loopi1(n) for (int i = 1; i <= n; i++)
#define loopj(n) for (int j = 0; j < n; j++)
#define loopj1(n) for (int j = 1; j <= n; j++)
#define mbr(n,m) get<m>(n)
#define DEBUG 0
#define CASES 0
#define endl "\n"
#define all(n) n.begin(), n.end()

int dp[TM], weight[TM];
int knapsack(uint amount, uint coins) {
    dp[0] = 0;
    loopi1(amount) {
        dp[i] = dp[i - 1] + 1;
        loopj(coins) {
            if (i -  weight[j] < 0) continue;
            // printf("[Debugger]~$ Amount: %-2d, %-2d (%-2d) | %-2d (%-2d)\n", i, dp[i - 1], i - 1, dp[i - weight[j]], i - weight[j]);
            dp[i] = min(dp[i], min(dp[i - 1], dp[i - weight[j]]) + 1);
        } // printf("[Debugger]~$ Amount: %-2d -> %d\n", i, dp[i]);
    } return dp[amount];
}
void real_process() {
    uint total, coins, smallestCoin = LM; cin >> coins >> total;
    loopi(coins) {
        cin >> weight[i];
        smallestCoin = min((int)smallestCoin, (int)weight[i]);
    } if (total == 0 || coins == 0) cout << "0\n";
    else if (total < smallestCoin) cout << "-1\n";
    else cout << knapsack(total, coins) << endl;
    return; // Impossible case -1 (Pending)
}

int main() {
    #ifndef DEBUG
    freopen("test_case.in", "r", stdin);
    freopen("test_case.out", "w", stdout);
    #endif
    cin.tie(nullptr) -> ios::sync_with_stdio(false);
    int t(1);
    if (CASES) cin >> t;
    while(t--) real_process();
    return 0;
}