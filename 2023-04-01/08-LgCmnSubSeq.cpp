#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <cmath>
// #include <ctype.h>
// #include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <sstream>
// #include <algorithm>
// #include <regex>
using namespace std;
typedef long long ll;
typedef unsigned int uint;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int _ = 0; _ < n; _++)
#define loopi(n) for (int i = 0; i < n; i++)
#define loopi1(n) for (int i = 1; i <= n; i++)
#define loopj(n) for (int j = 0; j < n; j++)
#define loopj1(n) for (int j = 1; j <= n; j++)
#define mbr(n,m) get<m>(n)
#define DEBUG 0
#define CASES 0
#define endl "\n"
#define all(n) n.begin(), n.end()

uint LgCmnSubSeq(string word, string comp) {
    uint longer = word.length(), // max(word.length(), comp.length()),
        shorter = comp.length(); // min(word.length(), comp.length());
    uint dp[longer][shorter];
    loopi(longer + 1) loopj(shorter + 1) {
        if (i == 0 || j == 0) { dp[i][j] = 0; continue; }
        if (word[i - 1] == comp[j - 1]) dp[i][j] = 1 + dp[i - 1][j - 1];
        else dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
        // printf("[Debugger]~$ posW:%-2d [%c] posC:%-2d [%c] | %d\n", i, word[i-1], j, comp[j-1], dp[i][j]);
    } return dp[longer][shorter];
}
void real_process() {
    string word, comp; cin >> word >> comp;
    cout << LgCmnSubSeq(word, comp) << endl;
    return;
    /***
     * [a][l]gor[i]thm[s]
     * [a]na[l]ys[i][s]
     ***/
}

int main() {
    #ifndef DEBUG
    freopen("test_case.in", "r", stdin);
    freopen("test_case.out", "w", stdout);
    #endif
    cin.tie(nullptr) -> ios::sync_with_stdio(false);
    int t(1);
    if (CASES) cin >> t;
    while(t--) real_process();
    return 0;
}