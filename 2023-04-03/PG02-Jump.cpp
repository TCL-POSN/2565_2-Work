#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <cmath>
// #include <ctype.h>
// #include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <sstream>
// #include <algorithm>
// #include <regex>
using namespace std;
typedef long long ll;
typedef unsigned int uint;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int _ = 0; _ < n; _++)
#define loopi(n) for (int i = 0; i < n; i++)
#define loopi1(n) for (int i = 1; i <= n; i++)
#define loopj(n) for (int j = 0; j < n; j++)
#define loopj1(n) for (int j = 1; j <= n; j++)
#define mbr(n,m) get<m>(n)
#define DEBUG 0
#define CASES 0
#define endl "\n"
#define all(n) n.begin(), n.end()

// Exceed time -> 60%
/* #define maxLines 30001
uint lines[maxLines];
void real_process() {
    int line, dist; uint maxJump = 1;
    cin >> line >> dist;
    loopi(line) cin >> lines[i];
    loopi(line) loopj1(line - i - 2) if (lines[i+j]-lines[i] >= dist) {
        // cout << "[Debugger]~$ L: " << 1+i << " -> " << 1+i+j << " (" << j-(lines[i+j]-lines[i] > dist ? 1 : 0) << "),\tR: " << lines[i] << " - " << lines[i+j] << " (" << lines[i+j]-lines[i] << ")" << endl;
        maxJump = max(maxJump, (uint)(j-(lines[i+j]-lines[i] > dist ? 1 : 0)));
        break;
    } cout << maxJump << endl;
    return;
} */

// Fine -> 0%
/* #define maxLines 30001
uint lines[maxLines];
void real_process() {
    int line, dist; uint maxJump = 1, passed = 1;
    cin >> line >> dist;
    loopi(line) { cin >> lines[i]; }
    vector<bool> visited(maxLines, false);
    queue<uint> visiting;
    visiting.push(0); visited[0] = true;
    while (!visiting.empty()) {
        uint current = visiting.front();
        visiting.pop();
        maxJump = max(maxJump, passed);
        passed = 1;
        loopi1(line - current) {
            if (lines[current + i] - lines[current] > dist) break;
            else if (!visited[current + i]) {
                visited[current + i] = true;
                visiting.push(current + i);
                passed += 1;
            }
        }
    } cout << maxJump << endl;
    return;
} */

// Fine -> 0%
/* #define maxLines 60001
uint lines[maxLines];
void real_process() {
    int line, dist; uint maxJump = 0;
    cin >> line >> dist;
    int tmp_in; loop(line) { cin >> tmp_in; lines[tmp_in] = tmp_in; }
    uint passed;
    for (uint b = line/2; b >= 1; b /= 2) {
        passed = 0;
        while (passed+b < line && lines[passed+b] <= dist) { lines[b] = passed; passed += b; }
        maxJump = max(maxJump, passed);
    } cout << maxJump/2 << endl;
    return;
} */

// Passed -> 100%
#define maxLines 30000
uint lines[maxLines];
void real_process() {
    int line, dist; uint maxJump = 0;
    cin >> line >> dist;
    loopi(line) cin >> lines[i];
    loopi(line) maxJump = max(maxJump, (uint)(upper_bound(lines, lines + line, lines[i] + dist) - lines - 1 - i));
    cout << maxJump << endl;
    return;
}

int main() {
    #ifndef DEBUG
    freopen("test_case.in", "r", stdin);
    freopen("test_case.out", "w", stdout);
    #endif
    cin.tie(nullptr) -> ios::sync_with_stdio(false);
    int t(1);
    if (CASES) cin >> t;
    while(t--) real_process();
    return 0;
}