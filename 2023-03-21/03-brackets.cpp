#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <cmath>
// #include <ctype.h>
// #include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <sstream>
// #include <algorithm>
// #include <regex>
using namespace std;
typedef long long ll;
typedef unsigned int uint;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int _ = 0; _ < n; _++)
#define loop1(n) for (int _ = 1; _ <= n; _++)
#define loopi(n) for (int i = 0; i < n; i++)
#define loopi1(n) for (int i = 1; i <= n; i++)
#define loopj(n) for (int j = 0; j < n; j++)
#define loopj1(n) for (int j = 1; j <= n; j++)
#define mbr(n,m) get<m>(n)
#define DEBUG 0
#define CASES 0
#define endl "\n"
#define all(n) n.begin(), n.end()

typedef struct stacked_list {
    int top;
    string children;
} slptr;
slptr *stackCreate() {
    slptr *tower = new slptr;
    tower -> top = -1;
    return tower;
}
void stackShow(slptr *tower) {
    cout << "Data in the stack:";
    if (tower -> top == -1) cout << " No data";
    else loopi(tower -> top + 1) cout << " " << tower -> children[i];
    cout << endl;
}
bool stackPush(slptr *tower, char value) {
    tower -> top += 1;
    tower -> children = tower -> children + value;
    return true;
}
bool stackIsEmpty(slptr *tower) {
    return (tower -> top == -1);
}
char stackTop(slptr *tower) {
    if (stackIsEmpty(tower)) return '\0';
    return tower -> children[tower -> top];
}
char stackPop(slptr *tower) {
    if (stackIsEmpty(tower)) return '\0';
    char holder = stackTop(tower);
    tower -> children = (tower -> children).substr(0, (tower -> top)--);
    return holder;
}
void stackEmpty(slptr *tower) {
    tower -> top = -1;
    tower -> children = "";
}
void stackClone(slptr *tower1, slptr *tower2) {
    tower2 -> top = tower1 -> top;
    tower2 -> children = tower1 -> children;
}
void stackDestroy(slptr *tower) {
    delete tower;
}

#define maxlen 256
const char bracketOpen[] = "([{", bracketClose[] = ")]}";
const int bracketType = 3;
void real_process() {
    bool isValid = true; string expression; cin >> expression;
    slptr *brackets = stackCreate(); int length = expression.length();
    // cout << "[Debugger]~$ Received: " << expression << endl;
    // cout << "[Debugger]~$ Received-Length: " << length << endl;
    loopi(length) {
        loopj(bracketType) {
            if (expression[i] == bracketOpen[j]) {
                stackPush(brackets, bracketClose[j]);
                // cout << "[Debugger]~$ Found: " << bracketOpen[j] << endl;
                break;
            } else if (expression[i] == bracketClose[j]) {
                if (stackTop(brackets) == bracketClose[j]) {
                    stackPop(brackets);
                    // cout << "[Debugger]~$ Match: " << bracketClose[j] << endl;
                break;
                } else {
                    isValid = false;
                    // cout << "[Debugger]~$ Found: " << expression[i] << "\tExpected: " << stackTop(brackets) << endl;
                    break;
                }
            }
        } if (!isValid) break;
    } cout << (isValid ? "Valid" : "Invalid") << endl;
    return;
}

int main() {
    #ifndef DEBUG
    freopen("test_case.in", "r", stdin);
    freopen("test_case.out", "w", stdout);
    #endif
    cin.tie(nullptr) -> ios::sync_with_stdio(false);
    int t(1);
    if (CASES) cin >> t;
    while(t--) real_process();
    return 0;
}