#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <cmath>
// #include <ctype.h>
// #include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <sstream>
// #include <algorithm>
// #include <regex>
using namespace std;
typedef long long ll;
typedef unsigned int uint;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int _ = 0; _ < n; _++)
#define loop1(n) for (int _ = 1; _ <= n; _++)
#define loopi(n) for (int i = 0; i < n; i++)
#define loopi1(n) for (int i = 1; i <= n; i++)
#define loopj(n) for (int j = 0; j < n; j++)
#define loopj1(n) for (int j = 1; j <= n; j++)
#define mbr(n,m) get<m>(n)
#define DEBUG 0
#define CASES 0
#define endl "\n"
#define all(n) n.begin(), n.end()
void getInt(char question[], int *answer) {
    // iostream in before out fix
	printf("\n%s ", question);
	scanf("%d", answer);
}
int randomVal() { return rand() % 99 + 1; }

#define maxSize 255
typedef struct stacked_list {
    int top;
    int children[maxSize];
} slptr;

// int tmp_counter;
slptr *stackCreate() {
    slptr *tower = new slptr;
    tower -> top = -1;
    return tower;
}
void stackShow(slptr *tower) {
    cout << "Data in the stack:";
    if (tower -> top == -1) cout << " No data";
    else loopi(tower -> top + 1) cout << " " << tower -> children[i];
    cout << endl;
}
bool stackPush(slptr *tower, int value) {
    if (tower -> top == maxSize) return false;
    tower -> children[++(tower -> top)] = value;
    return true;
}
bool stackIsEmpty(slptr *tower) {
    return (tower -> top == -1);
}
int stackTop(slptr *tower) {
    if (stackIsEmpty(tower)) return LM;
    return tower -> children[tower -> top];
}
int stackPop(slptr *tower) {
    if (stackIsEmpty(tower)) return LM;
    int holder = stackTop(tower);
    tower -> children[(tower -> top)--] = 0;
    return holder;
}
void stackEmpty(slptr *tower) {
    while (tower -> top > -1) tower -> children[(tower -> top)--] = 0;
}
void stackClone(slptr *tower1, slptr *tower2) {
    loopi(tower1 -> top + 1) tower2 -> children[i] = tower1 -> children[i];
    tower2 -> top = tower1 -> top;
}
void stackDestroy(slptr *tower) {
    delete tower;
}

void real_process() {
    // Creation
    slptr *work = stackCreate();
    stackShow(work);

    // Insertion
    loop(3) stackPush(work, randomVal());
    stackShow(work);
    
    // Displaying
    cout << "Top: " << stackTop(work) << endl;

    // Copying
    slptr *jobs = stackCreate();
    stackClone(work, jobs);
    stackShow(jobs);

    // Obliteration
    int popped = stackPop(work);
    cout << "Popped: " << popped << endl;
    stackShow(work);

    stackEmpty(work);
    stackShow(work);

    stackDestroy(jobs);

    return;
}

int main() {
    #ifndef DEBUG
    freopen("test_case.in", "r", stdin);
    freopen("test_case.out", "w", stdout);
    #endif
    cin.tie(nullptr) -> ios::sync_with_stdio(false);
    int t(1);
    if (CASES) cin >> t;
    while(t--) real_process();
    return 0;
}