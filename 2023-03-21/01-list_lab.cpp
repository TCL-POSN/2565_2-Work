#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <cmath>
// #include <ctype.h>
// #include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <sstream>
// #include <algorithm>
// #include <regex>
using namespace std;
typedef long long ll;
typedef unsigned int uint;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int _ = 0; _ < n; _++)
#define loop1(n) for (int _ = 1; _ <= n; _++)
#define loopi(n) for (int i = 0; i < n; i++)
#define loopi1(n) for (int i = 1; i <= n; i++)
#define loopj(n) for (int j = 0; j < n; j++)
#define loopj1(n) for (int j = 1; j <= n; j++)
#define mbr(n,m) get<m>(n)
#define DEBUG 0
#define CASES 0
#define endl "\n"
#define all(n) n.begin(), n.end()
void getInt(char question[], int *answer) {
    // iostream in before out fix
	printf("\n%s ", question);
	scanf("%d", answer);
}
int randomVal() { return rand() % 99 + 1; }

typedef struct linked_list {
    int info;
    struct linked_list *next;
} llptr;

int tmp_counter;
llptr *createList(int size, bool initial = true) {
    if (!size && !initial) return NULL;
    if (initial) tmp_counter = 1;
    llptr *node = new llptr; // (llptr *)malloc(sizeof(llptr));
    /* node -> info = randomVal();
    cout << "Input data for node " << tmp_counter++ << ": " << node -> info << endl; */
    printf("Input data for node %d: ", tmp_counter++); scanf("%d", &(node -> info));
    node -> next = createList(size - 1, false);
    return node;
}
void displayList(llptr *root, bool initial = true) {
    if (initial) {
        tmp_counter = 1;
        cout << "\n---Data in the list---\n";
    } cout << "Data = " << root -> info << endl;
    if (root -> next) displayList(root -> next, false);
}
llptr *insertFront(llptr *root, int value) {
    llptr *node = new llptr;
    node -> info = value;
    node -> next = root;
    return node;
}
llptr *insertBack(llptr *root, int value) {
    llptr *node = new llptr, *buffer = root;
    node -> info = value;
    node -> next = NULL;
    while (buffer -> next) buffer = buffer -> next;
    buffer -> next = node;
    return root;
}
llptr *insertMiddle(llptr *root, int value) {
    llptr *node = new llptr, *buffer = root, *holder = root;
    node -> info = value;
    tmp_counter = 1;
    while (buffer -> next) {
        buffer = buffer -> next;
        if (tmp_counter % 2 == 0) holder = holder -> next;
        tmp_counter++;
    } node -> next = holder -> next;
    holder -> next = node;
    return root;
}
llptr *deleteFront(llptr *root) {
    llptr *node = root -> next;
    delete root;
    return node;
}
llptr *deleteBack(llptr *root) {
    llptr *scout, *holder;
    scout = root -> next; holder = root;
    while (scout -> next) {
        holder = scout;
        scout = scout -> next;
    } delete scout;
    holder -> next = NULL;
    return holder;
}
llptr *deleteAt(llptr *root, int pos) {
    if (pos <= 1) return root;
    llptr *node = root -> next, *buffer = root;
    tmp_counter = 0;
    while (++tmp_counter < pos-1 && buffer -> next) buffer = buffer -> next;
    llptr *scout = buffer -> next;
    buffer -> next = scout -> next;
    delete scout;
    return root;
}
void searchList(llptr *root, int query) {
    /* Search by position */ /*
    llptr *buffer = root;
    tmp_counter = 0;
    while (++tmp_counter < query) buffer = buffer -> next;
    if (buffer) cout << "Data at node " << query << " is " << buffer -> info << endl;
    else cout << "Data at node " << query << " is not found" << endl; */
    /* Search by value */
    llptr *buffer = root;
    tmp_counter = 0;
    do {
        tmp_counter++;
        if (buffer -> info == query) {
            cout << "Data found at node " << tmp_counter << endl;
            query = NULL;
            break;
        } buffer = buffer -> next;
    } while (buffer);
    if (query != NULL) cout << "This data cannot be founded in this list" << endl;
}

void real_process() {
    // Creation (task 1)
    llptr *quarry;
    int tmp_in; getInt("Input the number of nodes:", &tmp_in);
    quarry = createList(tmp_in);
    displayList(quarry);

    // Insertion (task 2)
    getInt("Insert number in front:", &tmp_in);
    quarry = insertFront(quarry, tmp_in);
    displayList(quarry);
    
    getInt("Insert number in back:", &tmp_in);
    insertBack(quarry, tmp_in);
    displayList(quarry);

    getInt("Insert number in middle:", &tmp_in);
    insertMiddle(quarry, tmp_in);
    displayList(quarry);

    // Deletion (task 3)
    cout << "\nRemoving item in the front\n";
    quarry = deleteFront(quarry);
    displayList(quarry);

    cout << "\nRemoving item at the back\n";
    deleteBack(quarry);
    displayList(quarry);

    getInt("Delete number at position:", &tmp_in);
    deleteAt(quarry, tmp_in);
    displayList(quarry);

    // Searching (task 4)
    getInt("Input the element to be searched:", &tmp_in);
    searchList(quarry, tmp_in);

    return;
}

int main() {
    #ifndef DEBUG
    freopen("test_case.in", "r", stdin);
    freopen("test_case.out", "w", stdout);
    #endif
    cin.tie(nullptr) -> ios::sync_with_stdio(false);
    int t(1);
    if (CASES) cin >> t;
    while(t--) real_process();
    return 0;
}