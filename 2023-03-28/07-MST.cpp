#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <cmath>
// #include <ctype.h>
// #include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <sstream>
// #include <algorithm>
// #include <regex>
using namespace std;
typedef long long ll;
typedef unsigned int uint;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int _ = 0; _ < n; _++)
#define loopi(n) for (int i = 0; i < n; i++)
#define loopi1(n) for (int i = 1; i <= n; i++)
#define loopj(n) for (int j = 0; j < n; j++)
#define loopj1(n) for (int j = 1; j <= n; j++)
#define mbr(n,m) get<m>(n)
#define DEBUG 0
#define CASES 0
#define endl "\n"
#define all(n) n.begin(), n.end()

int places, edges;
vector<int> path; priority_queue<pii, vector<pii>, greater<pii>> lookup;
int origin(int pos) {
    if (path[pos] == pos) return pos;
    path[pos] = origin(path[pos]);
    return path[pos];
}
int kruskal() {
    int shortest = 0, dept, tmnl, dist;
    path.resize(places);
    loopi(places) path[i] = i;
    while (!lookup.empty()) {
        dist = lookup.top().st; dept = lookup.top().nd.st; tmnl = lookup.top().nd.nd;
        lookup.pop();
        if (origin(dept) == origin(tmnl)) continue;
        shortest += dist;
        path[origin(dept)] = path[origin(tmnl)];
    } return shortest;
}
void real_process() {
    cin >> places >> edges;
    int dept, tmnl, dist;
    loop(edges) {
        cin >> dept >> tmnl >> dist;
        lookup.push({dist, {dept, tmnl}});
    } cout << kruskal() << endl;
    return;
}

int main() {
    #ifndef DEBUG
    freopen("test_case.in", "r", stdin);
    freopen("test_case.out", "w", stdout);
    #endif
    cin.tie(nullptr) -> ios::sync_with_stdio(false);
    int t(1);
    if (CASES) cin >> t;
    while(t--) real_process();
    return 0;
}