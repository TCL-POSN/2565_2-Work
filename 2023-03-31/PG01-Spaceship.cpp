#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <cmath>
// #include <ctype.h>
// #include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <sstream>
// #include <algorithm>
// #include <regex>
using namespace std;
typedef long long ll;
typedef unsigned int uint;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int _ = 0; _ < n; _++)
#define loopi(n) for (int i = 0; i < n; i++)
#define loopi1(n) for (int i = 1; i <= n; i++)
#define loopj(n) for (int j = 0; j < n; j++)
#define loopj1(n) for (int j = 1; j <= n; j++)
#define mbr(n,m) get<m>(n)
#define DEBUG 0
#define CASES 0
#define endl "\n"
#define all(n) n.begin(), n.end()

typedef struct st_stock {
    uint scr, kbd, pc;
} stock;
typedef struct st_coord {
    int x, y, z;
    stock has;
} coord;
typedef struct st_history {
    uint place, dist;
    coord state;
    set<uint> visited;
} history;
uint build(stock bag) { return min(bag.scr, min(bag.kbd, bag.pc)); }
uint cost(coord dept, coord tmnl) { return (tmnl.x - dept.x) * (tmnl.x - dept.x) + (tmnl.y - dept.y) * (tmnl.y - dept.y) + (tmnl.z - dept.z) * (tmnl.z - dept.z); }
void real_process() {
    uint require, shops, best = LM;
    coord loc, pos; vector<coord> shop;
    queue<history> lookup; history tracker;
    cin >> require;
    cin >> loc.x >> loc.y >> loc.z;
    loc.has = {0, 0, 0};
    cin >> shops; loopi(shops) {
        cin >> pos.x >> pos.y >> pos.z;
        cin >> pos.has.scr >> pos.has.kbd >> pos.has.pc;
        shop.push_back(pos);
        lookup.push({(uint)i, 0, loc, {}});
    } while (!lookup.empty()) {
        tracker = lookup.front(); lookup.pop();
        // printf("[Debugger]~$ @(%-2d, %-2d, %-2d) -> (%-2d, %-2d, %-2d) [+%-6d = ", tracker.state.x, tracker.state.y, tracker.state.z, shop[tracker.place].x, shop[tracker.place].y, shop[tracker.place].z, cost(tracker.state, shop[tracker.place]));
        tracker.dist += cost(tracker.state, shop[tracker.place]);
        tracker.state.x += shop[tracker.place].x;
        tracker.state.y += shop[tracker.place].y;
        tracker.state.z += shop[tracker.place].z;
        tracker.state.has.scr += shop[tracker.place].has.scr;
        tracker.state.has.kbd += shop[tracker.place].has.kbd;
        tracker.state.has.pc += shop[tracker.place].has.pc;
        // printf("%-6d] {%-2d, %-2d, %-2d} => %-2d\n", tracker.dist, tracker.state.has.scr, tracker.state.has.kbd, tracker.state.has.pc, build(tracker.state.has));
        if (build(tracker.state.has) >= require) {
            best = min(best, tracker.dist);
            if (best == 1) break;
            continue;
        } else if (tracker.dist > best) continue;
        if (tracker.visited.size() + 1 == shops) continue;
        tracker.visited.insert(tracker.place);
        loopi(shops) {
            if (tracker.visited.find(i) != tracker.visited.end()) continue;
            lookup.push({(uint)i, tracker.dist, tracker.state, tracker.visited});
        }
    } cout << best;
    // 2 / 10
    return;
}

int main() {
    #ifndef DEBUG
    freopen("test_case.in", "r", stdin);
    freopen("test_case.out", "w", stdout);
    #endif
    cin.tie(nullptr) -> ios::sync_with_stdio(false);
    int t(1);
    if (CASES) cin >> t;
    while(t--) real_process();
    return 0;
}