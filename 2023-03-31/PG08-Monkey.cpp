#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <cmath>
// #include <ctype.h>
// #include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <sstream>
// #include <algorithm>
// #include <regex>
using namespace std;
typedef long long ll;
typedef unsigned int uint;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int _ = 0; _ < n; _++)
#define loopi(n) for (int i = 0; i < n; i++)
#define loopi1(n) for (int i = 1; i <= n; i++)
#define loopj(n) for (int j = 0; j < n; j++)
#define loopj1(n) for (int j = 1; j <= n; j++)
#define mbr(n,m) get<m>(n)
#define DEBUG 0
#define CASES 0
#define endl "\n"
#define all(n) n.begin(), n.end()

const int maxSize = 2e5 + 5;
vector<set<uint>> branch(maxSize, set<uint>());
void real_process() {
    uint height, pillars, branches; cin >> height >> pillars >> branches; bool puts, still;
    vector<uint> banana = {0}; uint pillar, stick, climbed = 0, most = 0, tmp_val;
    loop(pillars) { cin >> tmp_val; banana.push_back(tmp_val); } banana.push_back(0);
    loop(branches) { cin >> pillar >> stick; branch[pillar].insert(stick); }
    cin >> pillar; stack<pair<bool, pi>> joint; joint.push({false, {pillar, 0}});
    while (!joint.empty()) {
        pillar = joint.top().nd.st; climbed = joint.top().nd.nd;
        puts = joint.top().st; joint.pop();
        while (climbed != height) {
            climbed += 1;
            if (!puts && pillar - 1 > 0) joint.push({true, {pillar - 1, climbed}});
            if (!puts && pillar + 1 <= pillars) joint.push({true, {pillar + 1, climbed}});
            if (branch[pillar].find(climbed) != branch[pillar].end()) pillar += 1;
            else if (branch[pillar - 1].find(climbed) != branch[pillar - 1].end()) pillar -= 1;
        } if (banana[pillar] > most) {
            most = banana[pillar];
            still = puts;
        }
    } cout << most << endl << (still ? "USE" : "NO") << endl;
    // 9 / 20 - Timeouted
    return;
}

int main() {
    #ifndef DEBUG
    freopen("test_case.in", "r", stdin);
    freopen("test_case.out", "w", stdout);
    #endif
    cin.tie(nullptr) -> ios::sync_with_stdio(false);
    int t(1);
    if (CASES) cin >> t;
    while(t--) real_process();
    return 0;
}