#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <cmath>
// #include <ctype.h>
// #include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <sstream>
// #include <algorithm>
// #include <regex>
using namespace std;
typedef long long ll;
typedef unsigned int uint;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int _ = 0; _ < n; _++)
#define loopi(n) for (int i = 0; i < n; i++)
#define loopi1(n) for (int i = 1; i <= n; i++)
#define loopj(n) for (int j = 0; j < n; j++)
#define loopj1(n) for (int j = 1; j <= n; j++)
#define mbr(n,m) get<m>(n)
#define DEBUG 0
#define CASES 0
#define endl "\n"
#define all(n) n.begin(), n.end()

/* uint tmp_in, posX, posY;
uint Cnr(uint n, uint r) {
    if (r == 1) return n;
    if (r == 0 || r == n) return 1;
    uint answer = n; r = min(r, n - r);
    loopi1(r - 1) answer *= (n - i) / (r - i + 1);
    return answer;
}
void real_process() {
    uint roadY, roadX, dogs; cin >> roadY >> roadX >> dogs;
    vector<pi> junction; loop(dogs) { cin >> posX >> posY; junction.push_back({posX, posY}); }
    uint paths = Cnr(roadX + roadY, roadX);
    // Using combinatorics O() is growing up 2^n
    return;
} */

uint tmp_in, posX, posY;
const uint maxSize = 55;
bool junction[maxSize][maxSize];
void real_process() {
    uint roadY, roadX, dogs; cin >> roadX >> roadY >> dogs;
    loop(dogs) { cin >> posX >> posY; junction[posY][posX] = true; }
    stack<pi> path; path.push({1, 1});
    uint paths = 0; pi buffer;
    while (!path.empty()) {
        buffer = path.top(); path.pop();
        // printf("[Debugger]~$ Walking: %d @(%d, %d)\n", paths, buffer.st, buffer.nd);
        if (junction[buffer.nd][buffer.st]) continue;
        else if (buffer.st == roadX && buffer.nd == roadY) {
            paths += 1;
            continue;
        } if (buffer.st + 1 <= roadX) path.push({buffer.st + 1, buffer.nd});
        if (buffer.nd + 1 <= roadY) path.push({buffer.st, buffer.nd + 1});
    } cout << paths << endl;
    // 6 / 10 - Timeout
    return;
}

int main() {
    #ifndef DEBUG
    freopen("test_case.in", "r", stdin);
    freopen("test_case.out", "w", stdout);
    #endif
    cin.tie(nullptr) -> ios::sync_with_stdio(false);
    int t(1);
    if (CASES) cin >> t;
    while(t--) real_process();
    return 0;
}