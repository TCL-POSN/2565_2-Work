#include <stdio.h>
// #include <conio.h>
// #include <math.h>
// #include <cmath>
// #include <ctype.h>
// #include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <sstream>
// #include <algorithm>
// #include <regex>
using namespace std;
typedef long long ll;
typedef unsigned int uint;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int _ = 0; _ < n; _++)
#define loopi(n) for (int i = 0; i < n; i++)
#define loopi1(n) for (int i = 1; i <= n; i++)
#define loopj(n) for (int j = 0; j < n; j++)
#define loopj1(n) for (int j = 1; j <= n; j++)
#define mbr(n,m) get<m>(n)
#define DEBUG 0
#define CASES 0
#define endl "\n"
#define all(n) n.begin(), n.end()
void getInt(char question[], int *answer, bool continuous = false) {
    // iostream in before out fix
	printf("%c%s ", (continuous ? '\0' : '\n'), question);
	scanf("%d", answer);
}
int randomVal() { return rand() % 99 + 1; }

// ชัยนัดทำ max heap นะค้าา
class heap {
    int value, replica;
    heap *left, *right, *parent;
    public:
        heap();
        heap(int);
        heap(int, int);
        heap *insert(heap *, int, int);
        bool ordered(heap *);
        int pop(heap *);
}; int tmp_in, tmp_val;
heap::heap() :                      value(0),     replica(1),       left(NULL), right(NULL), parent(NULL) {}
heap::heap(int value) :             value(value), replica(1),       left(NULL), right(NULL), parent(NULL) {}
heap::heap(int value, int replica) :value(value), replica(replica), left(NULL), right(NULL), parent(NULL) {}
heap* heap::insert(heap* trunk, int value, int replica = 1) {
    if (!trunk) return new heap(value, replica);
    heap *buffer = trunk; queue<heap *> holder;
    while (buffer != NULL) {
        if (buffer -> value == value) { buffer -> replica += replica; break; }
        if (buffer -> left == NULL || buffer -> right == NULL) break;
        if (buffer -> left != NULL) holder.push(buffer -> left);
        if (buffer -> right != NULL) holder.push(buffer -> right);
        if (!holder.empty()) {
            buffer = holder.front();
            holder.pop();
        } else buffer = NULL;
    } if (buffer -> left == NULL) {
        buffer -> left = new heap(value, replica);
        buffer -> left -> parent = buffer;
    } else if (buffer -> right == NULL) {
        buffer -> right = new heap(value, replica);
        buffer -> right -> parent = buffer;
    }
    return trunk;
}
int heap::pop(heap *trunk) {
    if (trunk == NULL) return 0;
    const int removed = trunk -> value; heap *holder;
    if (trunk -> replica > 1) trunk -> replica -= 1;
    else if (trunk -> left == NULL && trunk -> right == NULL) delete trunk;
    else if (trunk -> left != NULL && trunk -> right == NULL) {
        holder = trunk -> left;
        holder -> parent = trunk -> parent;
        delete trunk; trunk = holder;
    } else if (trunk -> left == NULL && trunk -> right != NULL) {
        holder = trunk -> right;
        holder -> parent = trunk -> parent;
        delete trunk; trunk = holder;
    } else { // Failed
        heap *buffer = (trunk -> left -> value > trunk -> right -> value) ? trunk -> right : trunk -> left;
        holder = (trunk -> left -> value > trunk -> right -> value) ? trunk -> left : trunk -> right;
        delete trunk; trunk = holder;
        queue<heap *> task;
        while (buffer != NULL) {
            heap::insert(trunk, buffer -> value, buffer -> replica);
            if (buffer -> left != NULL) task.push(buffer -> left);
            if (buffer -> right != NULL) task.push(buffer -> right);
            delete buffer;
            if (!task.empty()) {
                buffer = task.front();
                task.pop();
            } else buffer = NULL;
        }
    } return removed;
}
bool heap::ordered(heap *trunk) {
    cout << "Order: ";
    if (trunk == NULL) {
        cout << "<Tree empty>" << endl;
        return false;
    } priority_queue<int, vector<int>, greater<int>> space; queue<heap *> holder; heap *buffer = trunk;
    while (buffer != NULL) {
        loop(buffer -> replica) space.push(buffer -> value);
        if (buffer -> left != NULL) holder.push(buffer -> left);
        if (buffer -> right != NULL) holder.push(buffer -> right);
        if (!holder.empty()) {
            buffer = holder.front();
            holder.pop();
        } else buffer = NULL;
    } while (!space.empty()) {
        cout << space.top() << " ";
        space.pop();
    } cout << endl;
    return true;
}


void real_process() {
    // buildHeap()
    getInt("Amount of data to add:", &tmp_val);
    getInt("Starting a heap tree with greatest value of:", &tmp_in);
    heap Heap, *tree = new heap(tmp_in);

    // insert()
    loop(tmp_val - 1) {
        getInt("Add data of value:", &tmp_in, true);
        Heap.insert(tree, tmp_in);
    } cout << endl; Heap.ordered(tree);

    // delete()
    getInt("Amount of data to delete:", &tmp_val);
    loop(tmp_val) cout << "Removed: " << Heap.pop(tree) << endl;
    cout << endl; Heap.ordered(tree);

    /***
     * 5
     * 20
     * 13
     * 7
     * 3
     * 4
     * 1
     * 
     ***/

    return;
}

int main() {
    #ifndef DEBUG
    freopen("test_case.in", "r", stdin);
    freopen("test_case.out", "w", stdout);
    #endif
    cin.tie(nullptr) -> ios::sync_with_stdio(false);
    int t(1);
    if (CASES) cin >> t;
    while(t--) real_process();
    return 0;
}