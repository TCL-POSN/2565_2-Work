#include <stdio.h>
// #include <conio.h>
#include <math.h>
// #include <ctype.h>
// #include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <sstream>
// #include <algorithm>
// #include <regex>
using namespace std;
typedef long long ll;
typedef unsigned int uint;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int _ = 0; _ < n; _++)
#define loop1(n) for (int _ = 1; _ <= n; _++)
#define loopi(n) for (int i = 0; i < n; i++)
#define loopi1(n) for (int i = 1; i <= n; i++)
#define loopj(n) for (int j = 0; j < n; j++)
#define loopj1(n) for (int j = 1; j <= n; j++)
#define mbr(n,m) get<m>(n)
#define DEBUG 0
#define CASES 0
#define endl "\n"
#define all(n) n.begin(), n.end()

unsigned int powerSum(int reach, int *power) {
    if (reach > 1) return pow(reach--, *power) + powerSum(reach, power);
    else return 1;
}

void real_process() {
    int upto, expo; cin >> upto >> expo;
    unsigned int result;
    switch (expo) {
        case 1: result = upto*(upto+1)/2; break;
        case 2: result = upto*(upto+1)*(2*upto+1)/6; break;
        case 3: result = pow(upto*(upto+1)/2, 2); break;
        case 4: result = upto*(upto+1)*(2*upto+1)*(3*pow(upto, 2)+3*upto-1)/30; break;
        case 5: result = pow(upto, 6)/6+pow(upto, 5)/2+5*pow(upto, 4)/12-pow(upto, 2)/12; break;
        case 6: result = pow(upto, 7)/7+pow(upto, 6)/2+pow(upto, 5)/2-pow(upto, 3)/6+upto/42; break;
        case 7: result = pow(upto, 8)/8+pow(upto, 7)/2+7*pow(upto, 6)/12-7*pow(upto, 4)/24+pow(upto, 2)/12; break;
        case 8: result = pow(upto, 9)/9+pow(upto, 8)/2+2*pow(upto, 7)/3-7*pow(upto, 5)/15+2*pow(upto, 3)/9-upto/30; break;
        default: result = powerSum(upto, &expo); break;
    } cout << result << endl;
    return;
}

int main() {
    #ifndef DEBUG
    freopen("test_case.in", "r", stdin);
    freopen("test_case.out", "w", stdout);
    #endif
    cin.tie(nullptr) -> ios::sync_with_stdio(false);
    int t(1);
    if (CASES) cin >> t;
    while(t--) real_process();
    return 0;
}