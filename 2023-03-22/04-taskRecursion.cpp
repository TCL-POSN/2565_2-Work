#include <stdio.h>
// #include <conio.h>
#include <math.h>
// #include <ctype.h>
// #include <stdlib.h>
#include <bits/stdc++.h>
// #include <string.h>
// #include <sstream>
// #include <algorithm>
// #include <regex>
using namespace std;
typedef long long ll;
typedef unsigned int uint;
typedef pair<int,int> pi;
typedef pair<int,pi> pii;
typedef pair<ll,ll> pl;
// #define INFint numeric_limits<int>::max()
// #define INFfloat numeric_limits<float>::infinity()
#define st first
#define nd second
const int MX = 2e9 + 7;
const int LM = INT_MAX;
const int TM = 1e6 + 2;
const int TT = 1e5 + 3;
const int MOD = 1e9 + 7;
const ll INF = 1e18;
const ll UI = 1e15;
#define loop(n) for (int _ = 0; _ < n; _++)
#define loop1(n) for (int _ = 1; _ <= n; _++)
#define loopi(n) for (int i = 0; i < n; i++)
#define loopi1(n) for (int i = 1; i <= n; i++)
#define loopj(n) for (int j = 0; j < n; j++)
#define loopj1(n) for (int j = 1; j <= n; j++)
#define mbr(n,m) get<m>(n)
#define DEBUG 0
#define CASES 0
#define endl "\n"
#define all(n) n.begin(), n.end()

int tmp_result;
int powerOf(int *base, int *expo) {
    // pow() from math.h bug fix
    auto powered = *base;
    loop(*expo-1) powered *= *base;
    return powered;
}
int resolve(int required, int *expo, int part = -1) {
    if (part == -1) part = (int)ceil(pow(required, 1.0/(*expo)));
    else if (part == 0) return 0;
    tmp_result = required - powerOf(&part, expo);
    if (tmp_result == 0) { tmp_counter++; return 1 + (part > 1 ? resolve(required, expo, part-1) : 0); }
    else if (tmp_result < 0) return resolve(required, expo, part-1);
    else return resolve(tmp_result, expo, part-1) + resolve(required, expo, part-1);
}

void real_process() {
    int total; int power; cin >> total >> power;
    cout << resolve(total, &power) << endl;
    return;
}

int main() {
    #ifndef DEBUG
    freopen("test_case.in", "r", stdin);
    freopen("test_case.out", "w", stdout);
    #endif
    cin.tie(nullptr) -> ios::sync_with_stdio(false);
    int t(1);
    if (CASES) cin >> t;
    while(t--) real_process();
    return 0;
}